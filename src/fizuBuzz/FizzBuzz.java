package fizuBuzz;

public class FizzBuzz {
	public static void main(String[] args) {

		for(int num = 0; num <100; num++) {

			FizzBuzz fizzBuzz = new FizzBuzz();
			fizzBuzz.fz(num);
		}
	}

		public String fz(int num) {

				//3の倍数の場合
				if(num % 3 == 0 && !(num % 5 == 0)) {
					return "Fizz";

					//5の倍数の場合
					} else if(num % 5 == 0 && !(num % 3 == 0)) {
						return "Buzz";

						//3と5の倍数の場合
						}else if(num % 15 == 0) {
							return "FizzBuzz";

							//その他の場合
							} else {
								return(Integer.toString(num));
								}

		}
}